def vowel_swapper(string):
    vowelswaps = {
        "a" : ["4", 0],
        "e" : ["3", 0],
        "i" : ["!", 0],
        "o" : ["ooo", 0],
        "u" : ["|_|", 0]
    }

    new_string = ""

    for i in range(0, len(string)):
        character = string[i]
        if character.lower() in vowelswaps:
            vowelswaps[character.lower()][1] += 1
            if vowelswaps[character.lower()][1] == 2:
                if character == "O":
                    new_string += "000"
                    continue
                else:
                    new_string += vowelswaps[character.lower()][0]
                    continue
        new_string += character

    return new_string


print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

