def factors(number):
    list = []
    for values in range(2, number):
        if number % values == 0:
            list.append(values)
    if len(list) != 0 or number == 1:
        return list
    else:
        return f"{number} is a prime number"

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
