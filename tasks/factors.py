def factors(number):
    list = []
    for num in range(2, round(number/2) +1):
        if number % num == 0:
            list.append(num)

    return list

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
